
## grpc client - server unary

./gradlew installDist
./build/install/examples/bin/hello-world-server
./build/install/examples/bin/hello-world-client


## grpc client -server streaming
./gradlew installDist
./build/install/examples/bin/route-guide-client
./build/install/examples/bin/route-guide-server


## java sampler for jmeter
Agregar esto como dependencia:

````
<dependency>
  <groupId>org.apache.jmeter</groupId>
  <artifactId>ApacheJMeter_core</artifactId>
  <version>${jmeter.core.version}</version>
  <exclusions>
      <exclusion>
          <groupId>com.sun</groupId>
          <artifactId>tools</artifactId>
      </exclusion>
      <exclusion>
          <groupId>commons-logging</groupId>
          <artifactId>commons-logging-api</artifactId>
      </exclusion>
  </exclusions>
</dependency>
````
##  install new maven version
ln -s /media/disquito/apache-jmeter-5.2/bin/jmeter.sh /usr/local/sbin/jmeter52

## Creating plugin 
````
cp proto files in ./src/main/proto
mvn clean install
cp /home/jennifer/.m2/repository/vn/zalopay/jmeter-grpc-client-sampler/0.1.0/jmeter-grpc-client-sampler-0.1.0.jar  /media/disquito/apache-jmeter-5.2/lib/ext/
````
## Creating Grpc Client Jar with Proto template files

Modify current env variable HEAP="-Xms1g -Xmx1g -XX:MaxMetaspaceSize=256m" in the jmeter batch file
Check : https://jmeter.apache.org/usermanual/best-practices.html

````
cd docs/example/grpc-lib
cp  proto.file ./src/main/proto
mvn install
mv target/grpc-lib-0.0.1.jar ../apache-jmeter-5.2/lib/ext/
````

## execute jmeter


````
export HEAP="-Xms1g -Xmx1g -XX:MaxMetaspaceSize=256m" && jmeter53 -n -t hello.jmx -l result_file_hello.csv
````

o
````
jmeter53 -n -t [jmx file] -l [results file] -e -o [Path to web report folder]
````