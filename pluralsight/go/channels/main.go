package main

import (
	"fmt"
	"sync"
)
func main() {
	wg := &sync.WaitGroup{}
	ch := make ( chan int, 1)

	wg.Add(2)
	//go func(ch chan int, wg *sync.WaitGroup) { // firma de channel bidireccional
	go func(ch chan int, wg *sync.WaitGroup) {	 // firma de channel
		//fmt.Println(<- ch) // receive message 42
		//close(ch)
		//what to do when a channel is close how to ask about channel state (open, close)
		// := operador coma
		//if msg, ok:= <-ch; ok { // when ok is false channel is closed. when ok is true the channel has a value.
		//	fmt.Println(msg, ok) // receive message 42
		//}
		//for
		//for i:=0; i<10; i++ {
		//	fmt.Println(<-ch)
		//}
		// otra forma de hacer el for sin utilizar constantes explicitas es:
		for  msg := range ch { // de esta forma se puede entrar en un dead lock si el channel nunca cierra. porque esta esperando datos
			fmt.Println(msg)
		}// puede ser un bucle infinito si no se cierra el channel
		wg.Done()
	}(ch, wg)
	
	go func(ch chan int, wg *sync.WaitGroup) {
		//envia  message in the channel a otra gorutina
		//ch <- 42
		//close(ch)
		//time.Sleep(1* time.Millisecond)
		//fmt.Println(<- ch) // receive message 27
		//for (sending in a for)
		for i:=0; i<10; i++ {
			ch <- i
		}
		close(ch)
		wg.Done()
	}(ch, wg)

	wg.Wait()
}
