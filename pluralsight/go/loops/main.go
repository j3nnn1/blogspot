package main

func main() {

		for i :=0; i< 5; i++ {
			println(i)

			if i==3 {
				break
			}
			println("continuing..." +
				" ")
		}
		// infinite loops
		// the ugly way
		//var i int
		//for ; ; {
		//	println(i)
		//
		//	if i==3 {
		//		break
		//	}
		//	println("continuing..." +
		//		" ")
		//}
		// the right way
		//for {
		//	println(i)
		//
		//	if i==3 {
		//		break
		//	}
		//}
	slice := []int{1,2,3}
	for i:=0; i< len(slice); i++ {
		println(slice[i])
	}
	for i, v := range slice {
		println(i, v)
	}
	for _, v := range slice {
		println(v)
	}
	panic("something bad just happened!")
	// effective go .html
}
