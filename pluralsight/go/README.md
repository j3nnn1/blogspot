
-- setting workspace in golang
-- we need three folders src, package and bin
-- recommended

- src
- pkg
- bin

== environment variable that say where my workspace is. ==
GOPATH => the root of workspace

== environment variables set  by GO ==

go env
go run hello-world.go 

== variables ==
- statically type
- at package level
- at function level²¹¹