package main

import (
	"fmt"
	"math/rand"
	"os"
	"reflect"
	"runtime"
	"time"
)

var (
	name, course string
	module       float64
)

func main() {
	/* inferencia de tipos.. hay que tener cuidado */
	a := 10.0
	b := 3
	module := 3.2
	c := int(a) + b
	ptr := &module

	fmt.Println("Hello from", runtime.GOOS)
	fmt.Println("Module is set to : ", module, "and is of type: ", reflect.TypeOf(name))
	fmt.Println("Name is set to :", name, "and is of type: ", reflect.TypeOf(module))
	fmt.Println("C is set to :", c, "and is of type: ", reflect.TypeOf(c))
	fmt.Println(" memory address of module variable is:  ", ptr, "and the value of module is: ", *ptr)

	name := os.Getenv("USERNAME")
	course := "Docker deep dive"

	fmt.Println("\n Hi, ", name, "you are currently watching  ", course)

	course = changeCourse(&course)

	fmt.Println("\n Hi, ", name, "you are currently watching  ", course)

	// fmt.Println("\n Hi, ",  os.Environ())
	// for _, env:= range os.Environ() {
	// 	fmt.Println(env)
	// }
	bestFinish := bestLeagueFinishes(13, 10, 13, 17, 14, 16, 7, 5, 2)
	fmt.Println("\n Hi, ", bestFinish)

	/* simple statement , se usa para inicializar variables que solo vas a usar dentro del if o ejecutar una sentencia corta antes de la evaluacion del IF*/
	if firstRank, secondRand := "39", "614"; firstRank < secondRand {
		fmt.Println("\n First course is doing better" + "than second course")
	} else if firstRank > secondRand {
		fmt.Println("\n Else if")
	} else {
		fmt.Println("\n Both are equal")
	}
	/* switch statement */
	/* comparing text strings */
	/* comparing same type variable */
	/* no necesita break; porque es implicito en el lenguaje , si quieres ese comportamiento del resto de los lenguajes tiene fallthrough sentence
	se agrega donde haga match y el resto de los cases siguientes se ejecutara igual a pesar de NO hacer match. NO se recomienda usarlo */

	switch "docker" {
	case "linux":
		fmt.Println("\n linux")
	case "docker":
		fmt.Println("\n docker")
		fallthrough
	case "windows":
		fmt.Println("\n windows")
	default:
		fmt.Println("\n no match")
	}

	switch tmpNum := random(); tmpNum {
	case 0, 2, 4, 6, 8:
		fmt.Println("\n even/ par: ", tmpNum)
	case 1, 3, 5, 7, 9:
		fmt.Println("\n odd/impar: ", tmpNum)
	default:
		fmt.Println("\n no match")
	}
	/* idiomatic go code */
	/* if err != nil  print some log or do something */

	/*infinite loops*/
	// for {
	// }
	// <expresions> can be boolean, range or nothing (infinite)
	// pre/ post statements xx; expresion; zzz
	for timer := 10; timer >= 0; timer-- {
		if timer == 0 {
			fmt.Println("BOOOM!")
			break
		}
		fmt.Println(timer)
		time.Sleep(1 * time.Second)

	}

	/* slice or list you need to use range: */
	/* range over it, ir por encima?? darle capacidad iterativa*/
	courseInProg := []string{"A", "B", "C"}
	courseInCompleted := []string{"A", "E", "D"}
	/* index value was ignore using _ */
	for _, i := range courseInProg {
		// fmt.Println(i)
		for _, j := range courseInCompleted {
			// fmt.Println(j)
			if i == j {
				fmt.Println(j)
				fmt.Println("is in both lists")
			}
		}
	}

	// break
	// break with label to a specific label
	// se suele usar en proyectos como docker

	//slices array
	myCourses := make([]string, 5, 10)

	fmt.Printf("Length is: %d. \n Capacity is: %d", len(myCourses), cap(myCourses))
	mySlice := make([]int, 1, 4)

	for i := 1; i < 17; i++ {
		mySlice = append(mySlice, i)
		fmt.Printf("\n Length is: %d. \n Capacity is: %d", len(mySlice), cap(mySlice))
	}

	mySlice = []int{1, 2, 3, 4, 5}

	for _, i := range mySlice {
		fmt.Printf("\n For range Loop: ", i)
	}
	newSlice := []int{10, 20, 30}
	mySlice = append(mySlice, newSlice...)

	fmt.Println(mySlice)

	// maps are unordered lists
	//defining

	leagueTitles := make(map[string]int)
	leagueTitles["Sunderland"] = 6
	leagueTitles["Newcastle"] = 4

	println(leagueTitles)

	recentHead2Head := map[string]int{
		"Sunderland": 5,
		"Newcastle":  0,
	}
	fmt.Printf("\n League tittles: %v\n Recent head to heads: %v \n ", leagueTitles, recentHead2Head)

	testMap := map[string]int{"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6}
	testMap["G"] = 1973
	testMap["A"] = 100
	for key, i := range testMap {

		fmt.Printf("\n key  is: %v, Values is: %v", key, i)
	}

	delete(testMap, "F")
	// maps are not safe to concurrency
	// are passed by reference

	type courseMeta struct {
		Author string
		Level  string
		Rating float64
	}

	// var DockerDeepDive courseMeta
	//DockerDeepDive := new(courseMeta)

	DockerDeepDive := courseMeta{
		Author: "Nigel Poulton",
		Level:  "Intermediate",
		Rating: 5,
	}

	fmt.Println(DockerDeepDive.Author)
}
func random() int {
	/* in functions is idiomatic or ok, return err variable, when this is null it's success otherwise failed. So always you should see a value variable and error variable*/
	rand.Seed(time.Now().Unix())
	return rand.Intn(10)
}
func changeCourse(course *string) string {
	*course = "First Look: Native Docker clustering"
	fmt.Println("\n triying to save your course to: ", *course)
	return *course
}

/* variadic function */
/* deal with any number de values */
func bestLeagueFinishes(finishes ...int) int {
	best := finishes[0]
	for _, i := range finishes {
		if i < best {
			best = i
		}
	}
	return best
}
