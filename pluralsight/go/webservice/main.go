package main

import (
	"gitlab.com/blogspot/pluralsight/go/webservice/controllers"
	"net/http"
)

type HTTPRequest struct {
	Method string
}
func main() {
	//r := HTTPRequest{ Method: "GET"}

	controllers.RegisterControllers()
	http.ListenAndServe(":3000", nil)
	//switch r.Method {
	//case "GET":
	//	println("Get Request")
	//	//fallthrough to add explicit break
	//case "DELETE":
	//	println("Delete Request")
	//case "POST":
	//	println("Post Request")
	//case "PUT":
	//	println("Put Request")
	//default:
	//	println("Unhandle method ")
	//}
}