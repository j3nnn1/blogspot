package main
import (
	"fmt"
	"sync"
	"time"
)

func main() {

	var waitGrp sync.WaitGroup
	waitGrp.Add(2) //quantity of go rutine

	// funciones anonimas que se ejecutan al declararse
	// go keyboard turn into gorutine
	go func() {
		defer waitGrp.Done()

		time.Sleep(5 * time.Second) // this lock the entire program
		fmt.Println("Hello")
	}()

	// funciones anonimas que se ejecutan al declararse
	// go keyboard turn into gorutine
	go func() {
		// si no coloco el defer para indicarle al proceso main sobre las go runtine genera este error:
		//
		// 		Pluralsight
		// Hello
		// fatal error: all goroutines are asleep - deadlock!
		// goroutine 1 [semacquire]:
		// sync.runtime_Semacquire(0xc42001a10c)
		defer waitGrp.Done()
		fmt.Println("Pluralsight")
	}()

	//wait to all go rutine finished ok.
	waitGrp.Wait()
}
