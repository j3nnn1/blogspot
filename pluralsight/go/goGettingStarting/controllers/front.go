package controllers

import (
	"net/http"
)

// RegisterControllers ...
func RegisterControllers() {
	uc := newUserController()

	// fooType := reflect.TypeOf(uc)
	// for i := 0; i < fooType.NumMethod(); i++ {
	// 	method := fooType.Method(i)
	// 	fmt.Println(method.Name)
	// }
	// fmt.Println(*uc)
	// fmt.Println(&uc)
	// fmt.Printf("%T\n", *uc)
	// fmt.Printf("%T\n", &uc)
	// fmt.Printf("%v", &uc)
	// fmt.Printf("%v", *uc)
	http.Handle("/users", *uc)
	http.Handle("/users/", *uc)
}
