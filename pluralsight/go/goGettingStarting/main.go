package main

import (
	"net/http"

	"gitlab.com/blogspot/pluralsight/goGettingStarting/controllers"
)

func main() {
	controllers.RegisterControllers()
	http.ListenAndServe(":3000", nil)

	// u := models.User{
	// 	ID:        2,
	// 	FirstName: "Tricia",
	// 	LastName:  "McMillian",
	// }
	// fmt.Println(u)
}
