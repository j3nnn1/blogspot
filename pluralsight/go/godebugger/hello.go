package main

import (
	"log"
	"net/http"
)

func main() {
	var i = 0
	log.Println(i)
	log.Println("starting server...")
	http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`Hello world`))
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}