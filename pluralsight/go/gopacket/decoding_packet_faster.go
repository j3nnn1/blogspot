
package main

import (
    "fmt"
    "github.com/google/gopacket"
    "github.com/google/gopacket/layers"
    "github.com/google/gopacket/pcap"
    "log"
    "time"
)

var (
    device       string = "wlp2s0"
    snapshot_len int32  = 5120
    promiscuous  bool   = false
    err          error
    timeout      time.Duration = 30 * time.Second
    handle       *pcap.Handle
    // Will reuse these for each packet
    ethLayer layers.Ethernet
    ipLayer  layers.IPv4
    tcpLayer layers.TCP
    udpLayer layers.UDP
)

func main() {
    // Open device
    handle, err = pcap.OpenLive(device, snapshot_len, promiscuous, timeout)
    if err != nil {
        log.Fatal(err)
    }
    defer handle.Close()

    packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
    for packet := range packetSource.Packets() {
        parser := gopacket.NewDecodingLayerParser(
            layers.LayerTypeEthernet,
            &ethLayer,
            &ipLayer,
            &tcpLayer,
            &udpLayer,
        )
        foundLayerTypes := []gopacket.LayerType{}

        err := parser.DecodeLayers(packet.Data(), &foundLayerTypes)
        if err != nil {
            fmt.Println("Trouble decoding layers: ", err)
        }

        for _, layerType := range foundLayerTypes {
            if layerType == layers.LayerTypeIPv4 {
                fmt.Println("IPv4: ", ipLayer.SrcIP, "->", ipLayer.DstIP)
            }
            if layerType == layers.LayerTypeTCP {
                fmt.Println("TCP Port: ", tcpLayer.SrcPort, "->", tcpLayer.DstPort)
                fmt.Println("TCP SYN:", tcpLayer.SYN, " | ACK:", tcpLayer.ACK)
            }
            if layerType == layers.LayerTypeUDP {
                // TOS, Length, Id, Flags, FragOffset, TTL, Protocol (TCP?),
                // Checksum, SrcIP, DstIP
                fmt.Println("UDP Length: ", udpLayer.Length, "->", udpLayer.DstPort)
                fmt.Println("udpLayer.SrcPort:", udpLayer.SrcPort, " | udpLayer.DstPort", udpLayer.DstPort)
                //fmt.Println("TCP Protocol:", udpLayer.Protocol, " | Id:", udpLayer.Id)
                fmt.Println("ES UDP Length siii")
                fmt.Println("udpLayer.CanDecode: ",  udpLayer.CanDecode())
                fmt.Println("udpLayer.LayerContents(): ",  udpLayer.LayerContents())
                fmt.Println("udpLayer.LayerPayload(): ",  udpLayer.LayerPayload())
                //data := udpLayer.Payload
                fmt.Println("udpLayer.DecodeFromBytes: ")
                var data = udpLayer.LayerPayload()

                //fmt.Println(udpLayer.DecodeFromBytes(data, )
                //fmt.Println(udpLayer.DecodeFromBytes(udpLayer.Contents))
            }
        }
    }
}
