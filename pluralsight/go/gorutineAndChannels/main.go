package main

import (
	"runtime"
)
func main() {
	runtime.GOMAXPROCS(8)
	doneCh := make(chan bool) //another channel to let us know when the or a or one go routine is done
	//main function is going to stay alive while we wont received the channel closed
	ch:= make(chan string)

	go abcGen(ch)
	go printer(ch, doneCh)
	println("this comes first!")

	//time.Sleep(100 * time.Millisecond)
	<-doneCh
}

func abcGen(ch chan string) {
	for l := byte('a'); l <= byte('z'); l++ {
		ch <- string(l) // send message
	}
	close(ch)
}
func printer(ch chan string, doneCh chan bool) {
	for l := range ch { // will exit when the channel is complete
		println(l) // receive message
	}

	//go routine finished!
	doneCh <- true
}