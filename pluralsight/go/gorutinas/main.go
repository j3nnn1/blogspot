
package main

import (
	"math/rand"
	"time"
	"fmt"
	"sync"
)

var  cache = map[int]Book{} //global variable exportable
var  rnd = rand.New(rand.NewSource(time.Now().UnixNano()))


func main () {
	wg := & sync.WaitGroup{}
	m := & sync.Mutex{}
	cacheCh := make (chan Book)
	dbCh := make (chan Book)

	// m := & sync.RWMutex{}
	// two types of mutex.
	// asimetrico el uso de ese elemento de memoria compartida recomienda usar RWMUTEX cuando tienen un monton de lecturas y pocas escrituras 
	// para la mayoria de los casos usar Mutex

	for i:=0; i<10; i++ {
		id := rnd.Intn(10) + 1
		wg.Add(2)
		//funcion anonima go rutina
		//        implicita
		go func (id int, wg *sync.WaitGroup, m *sync.Mutex, ch chan<- Book) {
			if b, ok := queryCache(id, m); ok {
				ch <- b
				//fmt.Println("from cache")
				//fmt.Println(b)
				//continue
			}
			wg.Done()
		}(id, wg, m, cacheCh)

		go func (id int,  wg *sync.WaitGroup, m *sync.Mutex, ch chan<- Book) {
		if b, ok := queryDatabase(id, m); ok {
			//fmt.Println("from database")
			//fmt.Println(b)
			m.Lock()
			cache[id] = b
			m.Unlock()
			ch <- b
			//continue
			wg.Done()
		}
		fmt.Printf("\nBook not found with id: %v \n", id)
		}(id, wg, m, dbCh)

		go func (cacheCh, dbCh <- chan Book) {
			select {
				case b:= <-cacheCh:
					fmt.Println("from cache")
					fmt.Println(b)
					<-dbCh
				case b:= <-dbCh:
					fmt.Println("from database")
					fmt.Println(b)

			}
		}(cacheCh, dbCh)
		time.Sleep(150 * time.Millisecond)
	}
	wg.Wait()
}

func queryCache(id int, m *sync.Mutex) (Book, bool) {
	m.Lock()
	b, ok := cache[id]
	m.Unlock()
	return b, ok
}

func queryDatabase(id int, m *sync.Mutex) (Book, bool) {
	time.Sleep(100 * time.Millisecond)
	for _, b:= range books {
		if b.ID == id {
			m.Lock() // la linea que quiero proteger  de error de memoria compartida la emvuelvo en lock y unlock
			cache[id] = b // error cuando dos gorutinas quieren acceder para leer y otro para escribir en esta variable global
			m.Unlock() // la linea que quiero proteger  de error de memoria compartida la emvuelvo en lock y unlock
			return b, true
		}
	}
	return Book{}, false
}
