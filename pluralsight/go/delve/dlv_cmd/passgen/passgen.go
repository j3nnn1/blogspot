package main

import (
	"log"
	"math/rand"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	alphanum    = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTSUVWXYZ23456789"
	symbols     = "~!@#$%^&*(){}[]<>?/"
	alphanumsym = alphanum + symbols
)

var (
	rnd = rand.New(rand.NewSource(time.Now().UnixNano()))
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		lenParam := r.URL.Query()["length"]
		length := 10
		if len(lenParam) > 0 {
			var err error
			length, err = strconv.Atoi(lenParam[0])
			if err != nil {
				length = 10
			}
		}
		symParam := r.URL.Query()["useSymbols"]
		useSymbols := true
		if len(symParam) > 0 {
			useSymbols = symParam[0] != "false"
		}
		pwd := genPassword(length, useSymbols)
		for _, v := range pwd {
			w.Write([]byte{v})
		}
	})

	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func genPassword(length int, useSymbols bool) []byte {
	result := make([]byte, length)
	order := make([]float64, len(result))
	var source string
	if useSymbols {
		source = alphanumsym
	} else {
		source = alphanum
	}

	for i := range result {
		result[i] = source[int(rnd.Float64()*float64(len(source)))]
		order[i] = rnd.Float64()
	}

	sort.Slice(result, func(l, r int) bool {
		return order[l] < order[r]
	})

	return result
}
