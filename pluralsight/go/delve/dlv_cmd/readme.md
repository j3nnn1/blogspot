- modo api:  dlv --api-version 
- modo backend: dlv --backend <string>
- cambiar el directorio de trabajo: --wd <string>
- usar N clientes (debug en equipo apuntando al mismo server):

- dlv debug passgen.go --headless (levanta el proceso y no esta usando la terminal)
    dlv debug passgen.go --headless 

Luego con el commando connect puedo enviar commandos a la api o servidor, cliento which attach to the server.
- dlv connect (detecta el servidor local e inicia el prompt de delve)
    
    dlv connect localhost:43463

- pasar un listado de comandos delve al iniciar el debug
    
    dlv debug passgen.go --init cmd.txt

- log flag, mas informacion del ambiente, en modo verbose delve
    dlv debug passgen.go --init cmd.txt --log


#### starting application
- debug (source + binario)
- exec (binario, pre binario, necesita flag al momento de hacer build para que no optimice la generacion del binario)
- test (test unitarios)
    - dlv test passgen 
    - dlv test packageName

- dlv attach (debugging in productoin o staging :O/ running processes) or  API / HTTP web

    - primero ejecuto el proceso web de go:
      -  go build -gcflags="-N -l"  passgen.go
      -  ./passgen
      -  buscar el pid ``ps -fea | grep passgen``
      -  en otra terminal: dlv attach <pid> | dlv attach 31732

dlv attach 31732

ERROR:
Could not attach to pid 28571: this could be caused by a kernel security setting, try writing "0" to /proc/sys/kernel/yama/ptrace_scope

SOLUTION:

1 - ) jennifer@jennifer-Lenovo-V330-14IKB:/code/blogspot/pluralsight/go/delve/dlv_cmd/passgen$ sudo su
root@jennifer-Lenovo-V330-14IKB:/code/blogspot/pluralsight/go/delve/dlv_cmd/passgen# echo "0" >/proc/sys/kernel/yama/ptrace_scope
root@jennifer-Lenovo-V330-14IKB:/code/blogspot/pluralsight/go/delve/dlv_cmd/passgen# exit
exit


2 - ) /home/jennifer/go/bin/dlv attach 28571
Type 'help' for list of commands.
(dlv) 

- dlv trace (debugging in productoin o staging  / running processes)

- dlv core (coredump in linux)

https://rakyll.org/coredumps/

- dlv replay

ejemplo para hacer debug de api de go:
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch GO",
            "type": "go",
            "request": "launch",
            "mode": "debug", //dlv debug
            "host":"127.0.0.1",
            "port":2345,
            "program": "${fileDirname}",
            "env": {},
            "args": [], //passgen: length
            "showLog": true
        }
    ]
}

- se necesita ejecutar un request al localhost para que ingrese en cada breakpoint.

graphical delve package:

go get -u github.com/aarzilli/gdlv

- cd Goproject
  
- gdlv debug

- muy similar a usar delve por linea de comando, pero usa headless tambien asi que se tiene que terminar correctamente con quit/exit.