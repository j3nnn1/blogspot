package main

import "testing"

func TestPasswordIsCorrectLength(t *testing.T) {
	const length = 42
	pwd := genPassword(length, false)
	if len(pwd) != length {
		t.Fatal("Password is not correct length")
	}
}
