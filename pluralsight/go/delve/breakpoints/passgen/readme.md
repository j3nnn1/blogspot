### make a script with delve command check cmd.txt:

break passgen.go:38
continue
locals

frame 0 locals

source pathfile
### commands to inspect a application state:

- variables and types
- source code (cpu register, mem)
- application state (big picture, inspect go rutine active) 

####  inspect a aplication state / variables and types:

- locals: variables in a scope
    locals ^re => locals expresion regular.

- vars
variables in all application. even the core libs.
    vars rnd
    
- print: imprimir el valor de una variable.
- args: argumentos que recibe la aplicacion.
     args ^re => locals expresion regular.
- types
    todos los tipos en la aplicacion, primitivos tipos, y tipos complejos.
- funcs 
    
    sin parametros adicionales se listan los todas las funciones disponibles en la aplicacion en ejecution, por esto es importante colocarle alguna expresion regular o el nombre de la funcion en si.

   funcs logger

#### source code / mem  and cpu register
- stack ver el orden de ejecucion de los diferentes archivos, como fluye por los diferentes estados la aplicacion.
  - stack
  - stack -full
- goroutines
  - cuando se trabaja con aplicaciones concurrentes, lista todas las gorutinas que estan activas en la aplicacion  y  cual linea fue ejecutada por cada gorutina e inspeccionar donde fue declarada la gorutina.
  - goroutines -g => de donde se ejecuto inicialmente la gorutina (pueden existir gorutinas huerfanas)
  - 
- threads:
- list
  - list obtiene el source code 5 lineas antes y despues de la linea actual a ejecutar.
  - list:81  obtiene el source code de la linea que se especifico.
- disassemble:
  - ver las instrucciones de assembler
- regs
  - ver cpu register de la linea en ejecucion.


#### manipulate state of the application during debug

- set: set variable (solo valores numericos y punteros)
- goroutine:  allow to change wich is the goroutine active in the application,when you use step thiw will move foward in this active goroutine.
  - goroutine <1|2|3|4|5>
- thread:  allow to change wich is the thread active in the application
  - thread <1|2|3|4|5>


Halting execution
- trace cantidad de veces que ha llegado a ese punto o break point
  -   trace foo main.go:6
- break delve se detiene en  esta linea, antes de ejecutar.
  - break main.go:6
- condition solo se detiene si se cumple la condicion, similar al break.
  - break main.go:6
  - condition 1<idBreadpoint> "foo"=="foo"

Setting a breakpoint:
**locspec.md**
    (dlv) break passgen.go:38
    (dlv) break function:line
    (dlv) break line
    (dlv) break main.main
    (dlv) clearall

#### setting condition in a breakpoint
    (dlv) break passgen.go:48
    (dlv) condition 1 *length == 10

#### trace command
  - how many times this loop is executed
  -  trace passgen.go:91
  -  goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)> goroutine(1): main.genPassword(10, true)The generated password is:
z%DgbT]X<8



### Restarting execution

continue
step
restart: reinicia el proceso de debug, all breakpoints will ve preserved
next (avoid step into a function)
stepout (get out of a function)

step-instruction :O assembly