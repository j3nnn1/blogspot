'use strict';

const PROTO_PATH = 'messages.proto';

const fs = require('fs');
const process = require('process');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
// const serviceDef = grpc.load(PROTO_PATH);
// https://www.npmjs.com/package/@grpc/proto-loader
const options = {};
const packageDefinition = protoLoader.loadSync(PROTO_PATH, options);
const serviceDef = grpc.loadPackageDefinition(packageDefinition);
// https://www.npmjs.com/package/@grpc/proto-loader

const PORT = 9000;

const cacert = fs.readFileSync('certs/ca.crt'),
      cert = fs.readFileSync('certs/client.crt'),
      key = fs.readFileSync('certs/client.key'),
      kvpair = {
          'private_key': key,
          'cert_chain': cert
      };
const creds = grpc.credentials.createSsl(cacert, key, cert);
const client = new serviceDef.EmployeeService(`localhost:${PORT}`, creds);

const option = parseInt(process.argv[2],10);

switch (option) {
    case 1:
        sendMetadata(client);
        break;
    case 2:
        // server client unary call
        getByBadgeNumber(client);
        break;
    case 3:
        // server side streaming response method
        getAll(client);
        break;
    case 4:
        // client side streaming calls => many messages to send a file by chunks of 64kb by default
        addPhoto(client);
        break;
    case 5:
        // bidirectional streaming calls
        saveAll(client);
        break;
}

function saveAll(client) {
    // bidirectional streaming calls
    const employees = [
        {
			badgeNumber: 123,
			firstName: "John",
			lastName: "Smith",
			vacationAccrualRate: 1.2,
			vacationAccrued: 0,
		},
		{
			badgeNumber: 234,
			firstName: "Lisa",
			lastName: "Wu",
			vacationAccrualRate: 1.7,
			vacationAccrued: 10,
		}
    ];

    const call = client.saveAll();
    call.on('data', function (emp) {
        // received from server
        // this always should be do at first because you can loose data(an employee) otherwise
        console.log(emp.employee);
    });
    employees.forEach(function (emp) {
        //sent by client to server in stream
        call.write({employee: emp});
    });
    // end of stream, there is no more employees
    call.end();
}

function addPhoto(client) {
    // doing a client streaming calls
    const md = new grpc.Metadata();
    md.add('badgenumber', '2080');

    const call = client.addPhoto(md, function (err, result) {
        console.log(result);
    });

    const stream = fs.createReadStream('Penguins.jpg');
    stream.on('data', function (chunk) {
        call.write({data: chunk});
    });
    stream.on('end', function () {
        call.end();
    });
}

function getAll(client) {
    // receiving a server streaming message
    const call = client.getAll({});

    call.on('data', function (data) {
        console.log(data.employee);
    });
}

function sendMetadata(client) {
    const md = new grpc.Metadata();
    md.add('username', 'mvansickle');
    md.add('password', 'password1');

    client.getByBadgeNumber({}, md, function () {});
}

function getByBadgeNumber(client) {
    // unary calls clients
    client.getByBadgeNumber({badgeNumber: 2080}, function (err, response) {
        if (err) {
            console.log(err);
        } else {
            console.log(response.employee);
        }
    });
}