# AWS cli

#### configure
```
aws --version
```

```
aws configure --profile testingNewKey
```

```
 aws configure list
      Name                    Value             Type    Location
      ----                    -----             ----    --------
   profile                <not set>             None    None
access_key                <not set>             None    None
secret_key                <not set>             None    None
```
#### create bucket
```
aws s3 mb s3://ps-sample-awsscp-1234567  --profile testingNewKey
```

#### list s3 buckets
```
aws s3 ls --profile testingNewKey
```

```
➜  aws-cli git:(master) ✗ aws s3 ls --profile testingNewKey
2020-09-02 21:34:36 ps-sample-awsscp-123456
```
#### upload files to aws

```
aws s3 sync  /media/disquito/code/j3nnn1/uva/dist/uva s3://ps-sample-awsscp-1234567 --profile testingNewKey
```

#### static s3 bucket for website 
```
aws s3api put-bucket-website --bucket ps-sample-awsscp-1234567 --website-configuration file://website.json --profile testingNewKey

aws s3 website s3://ps-sample-awsscp-123456/ --index-document index.html --error-document error.html --profile testingNewKey
```
#### list files in a bucket
```

aws s3 ls --profile testingNewKey


aws s3 ls   s3://ps-sample-awsscp-123456 --profile testingNewKey
```

### setting policies s3 bucket
#### acl hacer publico ese archivo y otros en ese bucket

```
aws s3api put-object-acl --bucket ps-sample-awsscp-1234567 --acl public-read --key index.html --profile testingNewKey
```

#### vaciar un bucket  

```
aws s3 rm s3://ps-sample-awsscp-1234567 --recursive --profile testingNewKey

aws s3 rm s3://ps-sample-awsscp-1234567/assets --recursive #(default credentials)
```

#### remove s3 bucket
```
aws s3 rb s3://ps-sample-awsscp-1234567 --force  --profile testingNewKey
```

#### get policies in s3

aws s3api get-object-acl --bucket ps-sample-awsscp-1234567 --key  index.html --profile testingNewKey


