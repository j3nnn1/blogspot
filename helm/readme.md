# making a local chart
# this make a template chart 
helm create jennichart

#dry run, to a service
helm install --dry-run --debug ./jennichart

#changing a port, overwrite config
helm install --dry-run --debug ./mychart --set service.internalPort=8080

#deploy a chart
helm install --debug ./jennichart

#access to nginx
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=jennichart,app.kubernetes.io/instance=precise-heron" -o jsonpath="{.items[0].metadata.name}")
echo "Visit http://127.0.0.1:8080 to use your application"
kubectl port-forward $POD_NAME 8080:80


#add chart repo

helm repo add gloo https://storage.googleapis.com/solo-public-helm
helm fetch --untar=true --untardir=. gloo/gloo

#install chart in a namespace
helm install gloo --namespace gloo-system -f gloo/values-knative.yaml

#linter al servicio
helm lint ./jennichart

#otro deploy
helm install jennichart --set service.type=NodePort