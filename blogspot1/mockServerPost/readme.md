# curl para probar el mock de un metodo POST / curl to test mock (POST method)

curl --location --request POST 'http://127.0.0.1:9991/test2' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=tigoqx' \
--data-urlencode 'password=DQ0lt4GC4SmuMe5OqiFk'


# Post para descargar una imagen / Post with a image
 curl  \           
-H 'Accept: application/xml, text/xml, application/json, application/*+xml, application/*+json' \
-H 'Content-Type: application/json' \
-H 'Host: localhost:9991' \
-H 'Connection: keep-alive' \
-X POST 'http://127.0.0.1:9991/test3' --output image.png

# GET Json Response
curl --location --request POST 'http://127.0.0.1:9991/api/cms' \
--header 'Content-Type: application/json' 


# Auth Json Post
curl \
-H 'Accept: application/xml, text/xml, application/json, application/*+xml, application/*+json' \
-H 'Content-Type: application/json' \
-H 'Host: localhost:9991' \
-H 'Connection: keep-alive' \
-H 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0aWdvcXgiLCJleHAiOjE2MDMzMTA3ODh9.Hf6mxsw5zvttSiui151tKBAJVKfPlP_FzmibhRVfRd1kZ_hkm5HtBeWWLs6oZtefL637gxdPOYigXAcNw9GcYg' \
-H 'Accept-Encoding: deflate, gzip' \
-X GET 'http://127.0.0.1:9991/api/test4?group=QoE-Bolivia&rows=100&offset=0' 

